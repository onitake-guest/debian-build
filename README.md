# debian-build container

This is just a simple container build file that sets up a Debian package build
environment with an unprivileged user. It's useful for quickly running tests
against multiple architectures, even if the necessary hardware isn't at hand.

CPU emulation is provided by Qemu, but the container can also be used for
native tests (obviously).

## Prerequisites

```shell
sudo apt install podman qemu-user-static
```

## Build

```shell
ARCH=amd64
podman build -t debian-build-${ARCH} --arch ${ARCH} .
```

## Use

### Unprivileged

```shell
ARCH=amd64
podman run -ti --rm --name debian-build-${ARCH}-mypackage --arch ${ARCH} debian-build-${ARCH}
git clone https://salsa.debian.org/.../mypackage
git-buildpackage -us -uc -b
```

### Privileged

For installing dependencies, etc.

Run the container as above, then do in a separate shell:

```shell
podman exec -ti -u 0 debian-build-${ARCH}-mypackage bash
apt update
apt install mydependency
```
